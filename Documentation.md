**Projet java :**

```java

package com.example.demo;
package helloservice.endpoint;

import javax.jws.WebService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestIsifidApplication {<>
	public class Fibonacci{  
	    	      
	        int nbr1=0, nbr2=1, nbr3, i, count=10;  
	        //print 0 and 1
	        System.out.print(nbr1+" "+nbr2);   
	    
	        //La boucle commence par 2 car 0 et 1 sont deja affiches
	        for(i=2; i<count; ++i)
	        {    
	            nbr3 = nbr1 + nbr2;       
	            nbr1 = nbr2;    
	            nbr2 = nbr3;  
	            System.out.print(" "+nbr3); 
	        }    
	  
	    }

	public class Fact {

    public static int fact (int n) {
        if (n==0) return(1);
        else return(n*fact(n-1));
    }
    
	public static endpoint create (@PathVariable int x) {
		if ( x < -1) {
			return ("400 Bad Request");
		}
		else if ((x/2)+1) {
			return Fibonacci;
		}
		else if (x>2 && x < 51) {
			return Fact;
		}
		else if (x > 2 && x > 50) {
			return x; 
		}
		
					
		
	}

	public static void main(String[] args) {
		SpringApplication.run(TestIsifidApplication.class, args);
	}

}

 ```
